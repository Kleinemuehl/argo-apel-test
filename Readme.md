# Overview

This project is intended to be a test setup for sending records to an APEL Server via
the Argo Messaging Service.
In a VM we bring up an [AMS broker](https://github.com/ARGOeu/argo-messaging)
with the accompanying [authentication mapper](https://github.com/ARGOeu/argo-api-authn)
and all dependencies. These services are exposed so that they can receive messages.
We also bring up an [APEL Server](https://github.com/apel/apel) which will recieve the
messages from the AMS broker via [SSM](https://github.com/apel/ssm).

## Dependencies
You need OpenSSL, [Ansible](https://www.ansible.com/) and [Vagrant](https://www.vagrantup.com/) installed.
Vagrant can work with different hypervisors (called providers), but the simplest setup is with [VirtualBox](https://www.virtualbox.org/).

## First time setup

### Certificates
You will need some x509 certificates.
These can be created using a quick-n-dirty script:
```
cd x509
./init.sh
```
This will create a root CA, an intermediary CA and endpoint certificates
`server` for the broker `ssm` as client certificate for the SSM and `client` to
authenticate yourself while sending to the AMS broker.

### VM
Now you can create and provision the VM with `vagrant up [--provider virtualbox]`.
After provisioning, comment the marked line in the `Vagrantfile` to
enable shared folders and bring up the machine again.
Connect to the VM using `vagrant ssh`.

### Installation in the VM
Go to `/vagrant/setup/` and run `./init.sh`.

## Running the test setup
### AMS Broker
From `/vagrant/setup/` you can bring up the broker using `docker compose up`.

### APEL Server
As the user `apel`, start `ssmreceive` and `apeldbloader`, both will run in the background.

The SSM receiver will poll the AMS broker for new messages and write them to
`/var/spool/apel/incoming` or `/var/spool/apel/reject`.

The DB loader will take messages from `/var/spool/apel/incoming` and put them into the running MariaDB.
The message will also be saved to `/var/spool/apel/accept`.

The one-shot command `apeldbunloader` will read table entries from the database and write them to `/var/spool/apel/outgoing` (where they normally would be picked up by another SSM).
The table is set in `/etc/apel/unloader.cfg`.

All commands write their logs to `/var/log/apel`.

### Sending Messages
To send records to the broker a sample script is provided `/vagrant/setup/send.sh`.
It should be run as `apel` or `root`.
After sending you should find the message in `/var/spool/apel/accept` and the locally running MariaDB.


