#!/bin/bash

AMS_PORT=443
AUTH_PORT=8443

client_pem=/etc/grid-security/client.pem
client_key=/etc/grid-security/client.key
capath=/etc/grid-security/ca
host=localhost

opts=$(getopt -o "" -l amsport:,cert:,key:,capath:,host: --name "$0" -- "$@")
eval set -- "$opts"
while true; do
    case "$1" in
        --cert)
            client_pem="$2"
            shift 2
            ;;
        --key)
            client_key="$2"
            shift 2
            ;;
        --capath)
            capath="$2"
            shift 2
            ;;
        --host)
            host="$2"
            shift 2
            ;;
        --amsport)
            AMS_PORT="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Unknown option: ${1}" 1>&2
            exit 1
            ;;
    esac
done

#set
#exit 0


# Job record or summary to send
_PAYLOAD=$(cat <<-EOF
APEL-individual-job-message: v0.3
Site: wuppertal
SubmitHost: wn1907
LocalJobId: 0
WallDuration: 3600
CpuDuration: 3600
StartTime: 1234
EndTime: 4321
ServiceLevel: 1
ServiceLevelType: hepspec
EOF
)

# Sign and encode our message
PAYLOAD=$(openssl smime -sign \
    -signer $client_pem \
    -inkey $client_key \
    -text <<< "${_PAYLOAD}")
PAYLOAD=$(base64 --wrap=0 <<< "${PAYLOAD}")

#echo "Payload to send: ${PAYLOAD}"
#TMP=$(openssl smime -pk7out <<< "${PAYLOAD}")
#echo "Test ${TMP}"
#TMP=$(openssl pkcs7 -print_certs <<< "${TMP}")
#echo "Test ${TMP}"

# json to send to argo
# what is empaid ?? ssm wants that
MSG=$(cat <<EOF
{
  "messages": [
  {
    "attributes":
    {
      "station":"Pleiades?",
      "status":"PROD",
      "empaid": "0"
    },
    "data":"${PAYLOAD}"
  }
  ]
}
EOF
)

# Obtain user token for ams
echo "Request user token..."
TOKEN=$(curl --capath $capath \
    --cert $client_pem \
    --key $client_key \
    -H "Content-Type: application/json" \
    -X GET "https://${host}:${AUTH_PORT}/v1/service-types/ams/hosts/ams:authx509" \
    | jq '.token' | tr -d '"')
echo -e "\n...obtained ${TOKEN}"

# Send message
echo "Send message"
curl --capath $capath \
    -H "Content-Type: application/json" \
    --data "${MSG}" \
    -X POST "https://${host}:${AMS_PORT}/v1/projects/accounting/topics/topic1:publish?key=${TOKEN}"


#echo "${MSG}"
echo ""
