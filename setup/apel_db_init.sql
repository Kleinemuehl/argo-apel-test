DROP DATABASE IF EXISTS test;
DROP DATABASE IF EXISTS apelserver;
CREATE DATABASE apelserver;

GRANT ALL ON apelserver.* TO 'apel'@'localhost';
SET PASSWORD FOR 'apel'@'localhost' = PASSWORD('apel');
FLUSH PRIVILEGES;

QUIT
