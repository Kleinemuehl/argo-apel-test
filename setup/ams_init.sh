#!/bin/bash
set -e
source .env

# Read DN out of a certificate, emulating AMS's format
read_DN() {
openssl x509 -subject -noout < $1 | awk '
BEGIN{FS="/"}
{
    printf("%s", $NF);
    for(i=NF-1; i>1; i--) {
        printf(",%s", $i);
    }
}'
}

ADMIN_TOKEN="b328c3861f061f87cbd34cf34f36ba2ae20883a5"
PROJECT_TOKEN="8169cf7bfc984b53efb2b177f5c126be58ae2f4f"
PROJECT_NAME="accounting"
PROJECT_ADMIN_NAME="accounting_admin"
TOPIC_NAME="topic1"
SUB_NAME="sub1"
CONSUMER_NAME="consumer"
PUBLISHER_NAME="publisher"
CONSUMER_UUID="consumer_UUID1"
PUBLISHER_UUID="publisher_UUID1"
CLIENT_DN="$(read_DN ${CERTS}/client.pem)"
SERVER_DN="$(read_DN ${CERTS}/server.pem)"
SSM_DN="$(read_DN ${CERTS}/ssm.pem)"
COMMON_ARGS=(--capath /etc/grid-security/ca  --header "Content-Type: application/json")

#echo $CLIENT_DN
#echo $SERVER_DN
#echo $SSM_DN
#exit 0

### Init DB
./mongo_init.sh

### Bring up broker
docker compose up --detach
while ! curl http://localhost:${AMS_PUBPORT} > /dev/null 2>&1; do sleep 0.5; done
while ! curl http://localhost:${AUTH_PUBPORT} > /dev/null 2>&1; do sleep 0.5; done

### Base AMS
echo -e "\nCreate topic"
curl -X PUT "${COMMON_ARGS[@]}" \
    "https://localhost:${AMS_PUBPORT}/v1/projects/${PROJECT_NAME}/topics/${TOPIC_NAME}?key=${PROJECT_TOKEN}"

echo -e "\nCreate subscription"
curl -X PUT "${COMMON_ARGS[@]}" \
    -d "{\"topic\": \"projects/${PROJECT_NAME}/topics/${TOPIC_NAME}\", \"ackDeadlineSeconds\":10}" \
    "https://localhost:${AMS_PUBPORT}/v1/projects/${PROJECT_NAME}/subscriptions/${SUB_NAME}?key=${PROJECT_TOKEN}"

echo -e "\nGive access to consumer"
curl -X POST "${COMMON_ARGS[@]}" \
    -d "{ \"authorized_users\":[\"${CONSUMER_NAME}\"] }" \
    "https://localhost:${AMS_PUBPORT}/v1/projects/${PROJECT_NAME}/subscriptions/${SUB_NAME}:modifyAcl?key=${PROJECT_TOKEN}"

echo -e "\nGive access to publisher"
curl -X POST "${COMMON_ARGS[@]}" \
    -d "{ \"authorized_users\":[\"${PUBLISHER_NAME}\"] }" \
    "https://localhost:${AMS_PUBPORT}/v1/projects/${PROJECT_NAME}/topics/${TOPIC_NAME}:modifyAcl?key=${PROJECT_TOKEN}"


### AUTH Service
echo -e "\nCreate service type"
UUID=$(curl -X POST "${COMMON_ARGS[@]}" \
    -d '{"name": "ams", "hosts": ["ams"],
    "auth_types": ["x509"], "auth_method": "api-key",
    "type": "ams"}' \
    "https://localhost:${AUTH_PUBPORT}/v1/service-types?key=${ADMIN_TOKEN}" \
    | jq '.uuid')
echo "... UUID is ${UUID}"

echo -e "\nCreate authentication method"
curl -X POST "${COMMON_ARGS[@]}" \
    -d "{\"access_key\": \"${ADMIN_TOKEN}\", \
    \"host\": \"ams\", \"port\": 8080}" \
    "https://localhost:${AUTH_PUBPORT}/v1/service-types/ams/authm?key=${ADMIN_TOKEN}"

echo -e "\nCreate consumer binding"
curl -X POST "${COMMON_ARGS[@]}" \
    -d "{\"name\": \"b_con\", \
    \"service_uuid\": ${UUID}, \
    \"host\": \"ams\", \
    \"auth_identifier\": \"${SSM_DN}\", \
    \"auth_type\": \"x509\", \
    \"unique_key\": \"${CONSUMER_UUID}\"}" \
    "https://localhost:${AUTH_PUBPORT}/v1/bindings/b_con?key=${ADMIN_TOKEN}"

echo -e "\nCreate publisher binding"
curl -X POST "${COMMON_ARGS[@]}" \
    -d "{\"name\": \"b_pub\", \
    \"service_uuid\": ${UUID}, \
    \"host\": \"ams\", \
    \"auth_identifier\": \"${CLIENT_DN}\", \
    \"auth_type\": \"x509\", \
    \"unique_key\": \"${PUBLISHER_UUID}\"}" \
    "https://localhost:${AUTH_PUBPORT}/v1/bindings/b_pub?key=${ADMIN_TOKEN}"

docker compose stop

echo ""

