import yaml
from jinja2 import Template
from os.path import splitext
import os
import sys
import re

mapping = {}

# process a single template file
def render(path):
    p = splitext(path)[0]
    with open(path, 'r') as fin:
        tmpl = Template(fin.read())
        out = tmpl.render(mapping)
        with open(p, 'w') as fout:
            fout.write(out)

# Read the mapping
with open('mongo.yml', 'r') as conf:
    mapping = yaml.safe_load(conf.read())

# Do we want to force-renew?
force = len(sys.argv) > 1 and sys.argv[1] == '-f'
            
# traverse dir tree starting from CWD
for dirpath, _, filenames in os.walk('.'):
    # filter for .j2 files
    for file in filter(lambda f: splitext(f)[1] == '.j2', filenames):
        tmpl = os.path.join(dirpath, file)
        f = splitext(tmpl)[0]
        # Only do something if the template is newer than the resulting file
        if force or not os.path.exists(f) or os.stat(f).st_mtime < os.stat(tmpl).st_mtime:
            # python 3.6 (centos7) does not have f-strings
            #print(f'Rendering {f}')
            print('Rendering {}'.format(tmpl))
            render(tmpl)
        else:
            print('Skipping {}'.format(tmpl))



