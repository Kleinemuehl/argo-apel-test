#!/bin/bash
set -e

pushd build/mongo
python3 render.py
popd

docker compose up --detach mongo
while ! curl http://localhost:27017 > /dev/null 2>&1; do
    sleep 0.5
done
docker exec ams-mongo-1 sh -c "mongo < /init_scripts/db_init.js"
docker compose stop

