#!/bin/bash

pushd /vagrant/ssm
sudo python setup.py install
popd

pushd /vagrant/apel
sudo python setup.py install
popd

source .env

# Place configs
sudo sh -c "openssl x509 -subject -noout < ${CERTS}/client.pem | sed 's/subject=\s*//' > /etc/apel/dns"
sudo cp /vagrant/setup/build/apel/*.cfg /etc/apel

sudo sh -c "echo '127.0.0.1 ams' >> /etc/hosts"

# SSM seems to ignore everything else, so place CAs here
sudo sh -c "cat ${CERTS}/ca/rootCA.pem >> ~apel/.local/lib/python2.7/site-packages/certifi/cacert.pem"
sudo sh -c "cat ${CERTS}/ca/interCA.pem >> ~apel/.local/lib/python2.7/site-packages/certifi/cacert.pem"

# Prepare Server database
sudo sh -c "mysql < apel_db_init.sql"
sudo sh -c "mysql apelserver < /vagrant/apel/schemas/server.sql"


