#!/bin/bash

rm {interCA,rootCA}/{,certs}/*
rm *.csr *.key *.pem *.crl
