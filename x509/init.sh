#!/bin/bash
set -e

[[ ! -d rootCA ]] && mkdir -p rootCA/certs
[[ ! -d interCA ]] && mkdir -p interCA/certs

echo 01 > rootCA/serial
echo 01 > interCA/serial
echo 01 > rootCA/crlnumber
echo 01 > interCA/crlnumber
touch rootCA/index
touch interCA/index

echo -e "\n########################################"
echo "##### Create Root CA"
echo "##### Type in the key that will be used for the root CA"
openssl req -x509 \
    -newkey rsa:4096 \
    -days 7300 \
    -config ca.conf \
    -extensions ext_rca \
    -keyout rootCA/rootCA.key \
    -out rootCA/rootCA.pem

echo -e "\n########################################"
echo "##### Create request for intermediate CA"
echo "##### Type in the key that will be used for the intermediate CA"
openssl req -new \
    -config ca.conf \
    -keyout interCA/interCA.key \
    -out interCA/interCA.csr

echo -e "\n########################################"
echo "##### Sign it"
openssl ca \
    -batch \
    -config ca.conf \
    -name root_CA \
    -extensions ext_ica \
    -in interCA/interCA.csr \
    -out interCA/interCA.pem

cert() {
    echo -e "\n########################################"
    echo "##### Create csr for ${1}"
    openssl req -new \
        -config ca.conf \
        -keyout "${1}.key" \
        -out "${1}.csr" \
        -nodes
    echo -e "\n########################################"
    echo "##### Sign it"
    openssl ca \
        -batch \
        -config ca.conf \
        -extfile "${1}.ext" \
        -in "${1}.csr" \
        -out "${1}.pem"
}

cert server
cert client
cert ssm

echo -e "\n########################################"
echo "##### Make empty CRL"
openssl ca \
    -gencrl \
    -config ca.conf \
    -out interCA/crl.pem

echo -e "\n########################################"
echo "##### Convert it to DER"
openssl crl \
    -in interCA/crl.pem \
    -outform DER \
    -out interCA.crl




